// function for showing/hiding header if user scroll down/up

(function($) {          
    $(document).ready(function() {    
        $(".top-nav-fixed").hide();                
        $(window).scroll(function(){                          
            if ($(this).scrollTop() > 500) {
                $('.top-nav-fixed').fadeIn(500);
            } else {
                $('.top-nav-fixed').fadeOut(500);
            }
        });
    });
})(jQuery);

// function for switching tabs

$(document).ready(function(e) {
    $(".nav div").click(function(e) {   
        $(".tab-content").hide();
        var selectedTab = $(this).find("a").attr("href");
        $(selectedTab).fadeIn(500);

        e.preventDefault();
    });
});


